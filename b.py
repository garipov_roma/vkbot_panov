import config
import urllib.request
import re
import vk_api
import time

vk_id = config.TOKEN

from bs4 import BeautifulSoup

def get_html(site):
	response = urllib.request.urlopen(site)
	return response.read().decode('utf8')

site = config.SITE

def parse(html):
	soup = BeautifulSoup(html, "lxml")
	wall = soup.find('div', class_ = "entry")
	wall = wall.find_all('p')
	return str(wall)
	
def get_schedule(h, g):
	h = h.replace('<p>', '')
	h = h.replace('</p>', '\n')
	h = h.replace('<br/>', '\n')
	h = h.replace(',', '')
	h = h.replace('[', '')
	h = h.replace(']', '')
	h = h.replace('\n ', '\n')
	h = h.replace('\n\n', '\n')
	h = list(h.split('—————————————————————————————-\n'))
	ind = h[0].find('\n')
	dt = ''
	for i in range(0, ind + 1):
		dt = dt + str(h[0][i])
	h[0] = h[0].replace(dt, '')
	n = len(h)
	l = list([])
	l.append(dt)
	for i in range(0, n):
		if (not(h[i].find(g) == -1)):
			l.append(h[i])
	res = ''
	for i in range(0, len(l)):
		if (i == len(l) - 1):
			res = res + l[i]
		else:
			res = res + l[i] + '\n'
	res = res[0:len(res) - 1]	
	return res
	
def get_groups():
	h = parse(get_html(site))
	h = h.replace('<p>', '')
	h = h.replace('</p>', '\n')
	h = h.replace('<br/>', '\n')
	h = h.replace(',', '')
	h = h.replace('[', '')
	h = h.replace(']', '')
	h = h.replace('\n ', '\n')
	h = h.replace('\n\n', '\n')
	h = list(h.split('—————————————————————————————-\n'))
	n = len(h)
	groups = set([])
	for i in range(1, n):
		tmp = h[i].split()[0]
		if (len(tmp) > 2):
			groups.add(tmp)
	return groups
	
def sch(g):
	h = parse(get_html(site))
	return (get_schedule(h, g))

#----------------PARSER
#----------------PARSER
#----------------PARSER

def write_msg(user_id, s):
    vk.method('messages.send', {'user_id':user_id,'message':s})

def send_bells(user_id):
	vk.method('messages.send', {'user_id':user_id, 'message' : 'Расписание звонков', 'attachment': ['photo170592770_456240514_9d95caa3c85e3aa026']})

vk = vk_api.VkApi(token = vk_id)

values = {'out': 0,'count': 100,'time_offset': 20}
vk.method('messages.get', values)

while True:
	response = vk.method('messages.get', values)
	if response['items']:
		values['last_message_id'] = response['items'][0]['id']
		#print(values)
		for item in response['items']:
			print(item)
			cur = item['body']
			user = item[u'user_id']
			if (cur == 'Помощь'):
				write_msg(user, config.msg1)
				continue
			if (cur == 'Список групп'):
				write_msg(user, '\n'.join(sorted(list(get_groups()))))
				continue
			if (cur == 'Старт'):
				write_msg(user, config.msg2)
				continue
			if (cur in get_groups()):
				write_msg(user, sch(cur))
				continue
			if (cur == 'Звонки'):
				send_bells(user)
				continue
			write_msg(user, config.msg3)
	time.sleep(1)

	
