pyTelegramBotAPI==3.2.1
requests==2.18.4
urllib3==1.22
beautifulsoup4==4.4.1
lxml==3.5.0
vk-api==9.1.1
certifi==2017.7.27.1
chardet==3.0.4
idna==2.6
six==1.11.0

